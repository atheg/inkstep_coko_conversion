require 'spec_helper'
require 'coko_conversion/ink_step/coko/mogrify_conversion_step'

describe InkStep::Coko::MogrifyConversionStep do

  let(:target_file_name)      { "kitty.jpg" }
  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }

  let(:input_directory)       { File.join(temp_directory, "input_files") }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
    @subject = InkStep::Coko::MogrifyConversionStep.new(chain_file_location: temp_directory, position: 1)
  end

  after :each do
    # FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do
    context 'with no options' do
      specify do
        expect_successful_conversion(test_subject: @subject)
      end
    end

    context 'with some options' do
      specify do
        @subject.combined_parameters = {options: "-crop 120x120+10+5"}

        expect_successful_conversion(test_subject: @subject)

        expect(@subject.process_log.grep(/Error running mogrify/).any?).to be_falsey
      end
    end
  end

  describe '#version' do
    specify do
      expect{@subject.version}.to_not raise_error
    end
  end

  describe '#description' do
    it 'has a description' do
      expect{@subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    it 'has a human readable name' do
      expect{@subject.class.human_readable_name}.to_not raise_error
    end
  end
end