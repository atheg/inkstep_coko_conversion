require 'spec_helper'
require 'coko_conversion/ink_step/coko/tidy_conversion_step'

describe InkStep::Coko::TidyConversionStep do

  let(:target_file_name)      { "some_text.html" }
  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }

  let(:input_directory)       { File.join(temp_directory, "input_files") }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
    @subject = InkStep::Coko::TidyConversionStep.new(chain_file_location: temp_directory, position: 1)
  end

  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do
    context 'converting a file successfully using Calibre' do
      specify do
        # subject.combined_parameters = {options: "‑clean"}
        expect_successful_conversion expected_output: [{:path=>"some_text.html", :size=>"276 bytes", :checksum=>"571d6310ddc7b1b03954fee4196ec716", :tag=>:modified}], test_subject: @subject
      end
    end
  end

  describe '#version' do
    specify do
      expect{@subject.version}.to_not raise_error
    end
  end

  describe '#description' do
    it 'has a description' do
      expect{@subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    it 'has a human readable name' do
      expect{@subject.class.human_readable_name}.to_not raise_error
    end
  end
end