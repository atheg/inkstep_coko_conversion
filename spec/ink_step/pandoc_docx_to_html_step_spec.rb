# encoding: utf-8
require 'spec_helper'
require 'coko_conversion/ink_step/coko/pandoc_docx_to_html_step'

describe InkStep::Coko::PandocDocxToHtmlStep do

  let(:docx_file_1)           { 'spec/fixtures/files/docx_1.docx' }
  let(:docx_file_2)           { 'spec/fixtures/files/docx_2.docx' }
  let(:docx_file_3)           { 'spec/fixtures/files/docx_3.docx' }
  let(:docx_file_4)           { 'spec/fixtures/files/docx_4.docx' }
  let(:input_directory)       { File.join(temp_directory, "input_files") }

  before do
    create_directory_if_needed(input_directory)
  end

  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do
    let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }

    before do
      FileUtils.cp(target_file, input_directory)
      @subject = InkStep::Coko::PandocDocxToHtmlStep.new(chain_file_location: temp_directory, position: 1)
    end

    context 'converting a file successfully using DocxToHTML (Pandoc)' do
      context 'first file' do
        let(:target_file_name)    { "docx_1.docx" }
        specify do
          expect_successful_conversion(test_subject: @subject)
        end
      end
      context 'second file' do
        let(:target_file_name)    { "docx_2.docx" }
        specify do
          expect_successful_conversion(test_subject: @subject)
        end
      end
      context 'third file' do
        let(:target_file_name)    { "docx_3.docx" }
        specify do
          expect_successful_conversion(test_subject: @subject)
        end
      end
      context 'fourth file' do
        let(:target_file_name)    { "docx_4.docx" }
        specify do
          expect_successful_conversion(test_subject: @subject)
        end
      end
    end
  end

  describe '#methods' do
    let(:target_file_name)    { "docx_1.docx" }
    let(:target_file)         { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }

    before do
      FileUtils.cp(target_file, input_directory)
      @subject = InkStep::Coko::PandocDocxToHtmlStep.new(chain_file_location: temp_directory, position: 1)
    end

    describe '#version' do
      it 'has a version' do
        expect{@subject.version}.to_not raise_error
      end
    end

    describe '#description' do
      it 'has a description' do
        expect{@subject.class.description}.to_not raise_error
      end
    end

    describe '#human_readable_name' do
      it 'has a human readable name' do
        expect{@subject.class.human_readable_name}.to_not raise_error
      end
    end
  end
end