# encoding: utf-8
require 'spec_helper'
require 'coko_conversion/ink_step/coko/pandoc_docx_to_html_step'
require 'coko_conversion/ink_step/coko/pandoc_epub_to_icml_step'

describe InkStep::Coko::PandocEpubToIcmlStep do

  let(:epub_file_1)           { 'spec/fixtures/files/epub_1.epub' }
  let(:epub_file_2)           { 'spec/fixtures/files/epub_2.epub' }
  let(:target_file_name)      { "document_1.epub" }
  let(:input_directory)       { File.join(temp_directory, "input_files") }
  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
    @subject = InkStep::Coko::PandocEpubToIcmlStep.new(chain_file_location: temp_directory, position: 1)
  end

  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do

    context 'converting a file successfully using EpubToICML (Pandoc)' do
      context 'first file' do

        specify do
          expect_successful_conversion(test_subject: @subject)
        end
      end
    end
  end

  describe '#version' do
    specify do
      expect{@subject.version}.to_not raise_error
    end
  end

  describe '#description' do
    it 'has a description' do
      expect{@subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    it 'has a human readable name' do
      expect{@subject.class.human_readable_name}.to_not raise_error
    end
  end
end