# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'coko_conversion/version'
Gem::Specification.new do |spec|
  spec.name           = 'inkstep_coko_conversion'
  spec.version        = CokoConversion::VERSION
  spec.date           = '2016-11-24'
  spec.summary        = "Contains steps that can convert to PDF, epub and HTML"
  spec.description    = "Contains steps using Vivliostyle and wkhtmltopdf for conversion to PDF; calibre to epub; and Pandoc and XSweet Pipeline from Docx to HTML."
  spec.authors        = ["Charlie Ablett"]
  spec.email          = 'charlie@coko.foundation'
  spec.homepage       = 'https://gitlab.coko.foundation/INK/inkstep-coko-conversion'
  spec.license        = 'MIT'
  spec.executables    = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files     = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths  = %w(lib)
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "webmock"
  spec.add_development_dependency "httparty"
  spec.required_ruby_version = '~> 2.2'
  spec.files         = Dir.glob("{lib}/**/*") + %w(./README.md)
end