require 'open3'
require 'ink_step/conversion_error'

module Utilities
  module SaxonXslMethods
    attr_accessor :xsl_file_path, :original_file_path, :saxon_jar_path

    # not my favourite thing, to introduce a Java dependency!
    # I'd like to use the C version directly via command line instead.
    # There's also a node.js port incoming
    # https://github.com/Saxonica/Saxon-CE/issues/1

    def apply_xslt_transformation(input_file_path:, output_file_path:, xsl_file_path:, provided_saxon_jar_path:)
      @original_file_path = input_file_path
      @xsl_file_path = xsl_file_path
      @saxon_jar_path = provided_saxon_jar_path || default_saxon_jar_path

      # http://www.saxonica.com/html/documentation/using-xsl/commandline.html
      # e.g.
      # java -jar Saxon-HE-9.7.0-4.jar -s:some_file.docx -xsl:xsl_file_path.xsl -o:conversion_output.html

      log_as_step "Applying xsl..."
      command = "java -jar #{Shellwords.escape(saxon_jar_path)} -s:#{Shellwords.escape(@original_file_path)} -xsl:#{Shellwords.escape(xsl_file_path)} -o:#{Shellwords.escape(output_file_path)}"
      log_as_step "Running command '#{command}'"

      Open3.popen3(command) do |stdin, stdout, stderr, wait_thr|
        exit_status = wait_thr.value
        @success = exit_status.success?
        @notes << stdout.read
        unless @success
          err = stderr.read
          log_as_step "err: #{err}"
          @errors << err
        end
      end

      # log_as_step "Does output file exist? #{File.exist?(output_file_path)}"
      if @success
        File.open(output_file_path)
      else
        # if not successful, halt the recipe at this step.
        raise InkStep::ConversionError.new(@errors.inspect)
      end
    end

    def default_saxon_jar_path
      root = File.expand_path '../..', File.dirname(__FILE__)
      File.join(root, "coko_conversion", "utilities", "saxon9he.jar")
    end
  end
end
