require 'coko_conversion/version'
require 'ink_step/conversion_step'

module InkStep::Coko
  class WkHtmlToPdfStep < InkStep::ConversionStep

    def perform_step
      super
      source_file_path = find_source_file(regex: parameter(:regex))
      source_file_name = Pathname(source_file_path).sub_ext ''
      output_file_path = File.join(working_directory, "#{source_file_name}.pdf")

      run_html_to_pdf_conversion(File.join(working_directory, source_file_name), output_file_path)
      success!
    end

    def version
      CokoConversion::VERSION
    end

    def self.description
      "Runs wkhtmltopdf conversion html to pdf. Template: wkthtmltopdf <input file> <output file>"
    end

    def self.human_readable_name
      "wkhtmltopdf HTML to PDF converter"
    end

    def required_parameters
      # e.g. [:foo, :bar]
      []
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {regex: "Regular expression for input file"}
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {regex: [/\.html$/, /\.htm$/]}
    end

    private

    def run_html_to_pdf_conversion(source_file_path, destination_file_path)
      # see readme for more info about usage

      # wkhtmltopdf input.html output.pdf
      command = "xvfb-run -a wkhtmltopdf #{Shellwords.escape(source_file_path)} #{Shellwords.escape(destination_file_path)}"

      perform_command(command: command, error_prefix: "Error running wkhtmltopdf")
    end
  end
end