require 'coko_conversion/version'
require 'ink_step/conversion_step'

module InkStep::Coko
  class PandocConversionStep < InkStep::ConversionStep

    def perform_step
      output_format = parameter(:output_format)
      input_format = parameter(:input_format)
      source_file_path = find_source_file(regex: [/\.#{input_format}$/])
      source_file_name = Pathname(source_file_path).sub_ext ''
      output_file_path = File.join(working_directory, "#{source_file_name}.#{output_format}")

      perform_conversion(File.join(working_directory, source_file_path), output_file_path, input_format, output_format)
      success!
    end

    def version
      CokoConversion::VERSION
    end

    def self.description
      "Converts target file via pandoc. Template: pandoc <input file> -f <input format> -t <output format> -s -o <output file>"
    end

    def self.human_readable_name
      "Generic Pandoc Converter"
    end

    def required_parameters
      # e.g. [:foo, :bar]
      [:output_format, :input_format]
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {input_format: "The current format of the file", output_format: "The format the file will be converted to."}
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {}
    end

    def perform_conversion(source, destination, input_format, output_format)
      # see readme for more info about usage
      command = "pandoc #{Shellwords.escape(source)} -f #{input_format} -t #{output_format} -s -o #{Shellwords.escape(destination)}"

      perform_command(command: command, error_prefix: "Error running pandoc")
    end
  end
end