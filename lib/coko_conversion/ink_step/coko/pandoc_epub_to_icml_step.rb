require 'coko_conversion/version'
require 'ink_step/conversion_step'
require 'coko_conversion/ink_step/coko/pandoc_conversion_step'

module InkStep::Coko
  class PandocEpubToIcmlStep < PandocConversionStep

    def perform_step
      super
      success!
    end

    def self.description
      "Converts target .epub file to .icml (for use in InDesign)"
    end

    def version
      CokoConversion::VERSION
    end

    def self.human_readable_name
      "Pandoc Epub to ICML converter"
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {
          input_format: "epub",
          output_format: "icml"
      }
    end
  end
end