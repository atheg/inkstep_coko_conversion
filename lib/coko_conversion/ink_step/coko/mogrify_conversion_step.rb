require 'coko_conversion/version'
require 'ink_step/conversion_step'

module InkStep::Coko
  class MogrifyConversionStep < InkStep::ConversionStep

    def perform_step
      source_file = parameter(:source_file) || find_source_file(regex: /\.jpg$/)
      output_file_name = parameter(:output_file)

      perform_conversion(source_file, output_file_name)
      success!
    end

    def version
      CokoConversion::VERSION
    end

    def self.description
      "Runs ImageMagick's Mogrify terminal command against an image file. The input file is modified. Template: mogrify <options> <input file> <output file>"
    end

    def self.human_readable_name
      "Generic Mogrify converter"
    end

    def required_parameters
      # e.g. [:foo, :bar]
      [:options]
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {
          options: "Any options for the conversion (see https://www.imagemagick.org/script/mogrify.php)"
      }
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {options: ""}
    end

    private

    def perform_conversion(source_file, output_file)
      output_file_path = output_file ? Shellwords.escape(File.join(working_directory, output_file)) : nil
      # Modifies the supplied image file
      # magick mogrify -resize 50% rose.jpg
      # ubuntu doesn't need the 'magick' bit
      command = "mogrify #{parameter(:options)} #{Shellwords.escape(File.join(working_directory, source_file))} #{output_file_path}"

      perform_command(command: command, error_prefix: "Error running mogrify")
    end
  end
end