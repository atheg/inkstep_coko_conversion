require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon'

module InkStep::Coko
  module XsweetPipeline
    module DocxExtract
      class JoinElementsStep < XsweetPipeline::DownloadAndExecuteXslViaSaxon

        def perform_step
          super
          success!
        end

        def remote_xsl_location
          "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/docx-extract/join-elements.xsl"
        end

        def self.description
          "Joins identical elements (e.g. <span>the </span><span>fish</span> => <span>the fish</span>)"
        end

        def self.human_readable_name
          "Xsweet Elements Joiner"
        end
      end
    end
  end
end
