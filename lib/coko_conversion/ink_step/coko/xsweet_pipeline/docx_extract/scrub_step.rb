require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon'

module InkStep::Coko
  module XsweetPipeline
    module DocxExtract
      class ScrubStep < XsweetPipeline::DownloadAndExecuteXslViaSaxon

        def perform_step
          super
          success!
        end

        def self.description
          "Removes superfluous elements (e.g. <i/>)"
        end

        def self.human_readable_name
          "Xsweet Scrubber"
        end

        def remote_xsl_location
          "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/docx-extract/scrub.xsl"
        end
      end
    end
  end
end
