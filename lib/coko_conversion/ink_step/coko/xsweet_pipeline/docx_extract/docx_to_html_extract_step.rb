require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon_on_docx'

module InkStep::Coko
  module XsweetPipeline
    module DocxExtract
      class DocxToHtmlExtractStep < DownloadAndExecuteXslViaSaxonOnDocx

        def perform_step
          super
          success!
        end

        def self.description
          "Extracts content from a .docx file to a HTML Typescript-conforming .html file"
        end

        def self.human_readable_name
          "Xsweet Docx to HTML Extractor"
        end

        def remote_xsl_location
          "https://gitlab.coko.foundation/atheg/XSweet/raw/master/applications/docx-extract/docx-html-extract.xsl"
        end
      end
    end
  end
end
