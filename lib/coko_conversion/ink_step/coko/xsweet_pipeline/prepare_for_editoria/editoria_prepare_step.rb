require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon'

module InkStep::Coko
  module XsweetPipeline
    module PrepareForEditoria
      class EditoriaPrepareStep < XsweetPipeline::DownloadAndExecuteXslViaSaxon

        def perform_step
          source_file_relative_path = find_source_file(regex: parameter(:regex))
          source_file_path = File.join(working_directory, source_file_relative_path)

          log_as_step "splitting ps on breaks in #{source_file_relative_path} to make #{p_split_on_br_done_path}..."
          download_file(parameter(:p_split_on_br_uri))
          apply_xslt_transformation(input_file_path: source_file_path,
                                    output_file_path: p_split_on_br_done_path,
                                    xsl_file_path: xsl_file_path,
                                    provided_saxon_jar_path: nil)

          reset_xsl_path

          log_as_step "reformatting notes on #{p_split_on_br_done_path} to make #{notes_done_path}..."
          download_file(parameter(:editoria_notes_xsl_uri))
          apply_xslt_transformation(input_file_path: p_split_on_br_done_path,
                                    output_file_path: notes_done_path,
                                    xsl_file_path: xsl_file_path,
                                    provided_saxon_jar_path: nil)

          reset_xsl_path

          log_as_step "running typescript basic on #{notes_done_path}..."
          download_file(parameter(:editoria_basic_xsl_uri))
          apply_xslt_transformation(input_file_path: notes_done_path,
                                    output_file_path: basic_done_path,
                                    xsl_file_path: xsl_file_path,
                                    provided_saxon_jar_path: nil)

          reset_xsl_path

          log_as_step "applying editoria reduce xslt to #{basic_done_path}..."
          download_file(parameter(:editoria_reduce_xsl_uri))
          apply_xslt_transformation(input_file_path: basic_done_path,
                                    output_file_path: reduced_done_path,
                                    xsl_file_path: xsl_file_path,
                                    provided_saxon_jar_path: nil)

          reset_xsl_path

          log_as_step "serializing to html5 xslt on #{reduced_done_path}..."
          download_file(parameter(:html5_serialize_xsl_uri))
          apply_xslt_transformation(input_file_path: reduced_done_path,
                                    output_file_path: source_file_path,
                                    xsl_file_path: xsl_file_path,
                                    provided_saxon_jar_path: nil)
          success!
        end

        def self.description
          "Prepares document to be used by Editoria"
        end

        def self.human_readable_name
          "Xsweet Editoria Preparer"
        end

        def required_parameters
          # e.g. [:foo, :bar]
          [
              :p_split_on_br_uri,
              :editoria_notes_xsl_uri,
              :editoria_basic_xsl_uri,
              :editoria_reduce_xsl_uri,
              :html5_serialize_xsl_uri
          ]
        end

        def accepted_parameters
          # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
          {
              p_split_on_br_uri: "Location of raw XSL file to download - splits <br> as <p>",
              editoria_notes_xsl_uri: "Location of raw XSL file to download - handles editoria notes",
              editoria_basic_xsl_uri: "Location of raw XSL file to download - basic HTML replacements",
              editoria_reduce_xsl_uri: "Location of raw XSL file to download - reducer",
              html5_serialize_xsl_uri: "Location of raw XSL file to download - html5 serializer",
              regex: "Regular expression for input file"
          }
        end

        def default_parameter_values
          # e.g. {foo: 1, bar: nil}
          {
              p_split_on_br_uri: "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/p-split-around-br.xsl",
              editoria_notes_xsl_uri: "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/editoria-notes.xsl",
              editoria_basic_xsl_uri: "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/editoria-basic.xsl",
              editoria_reduce_xsl_uri: "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/editoria-reduce.xsl",
              html5_serialize_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/html-polish/html5-serialize.xsl",
              regex: [/\.html$/, /\.htm$/]
          }
        end

        def p_split_on_br_done_path
          File.join(working_directory, "p_split_on_br_done.html")
        end

        def notes_done_path
          File.join(working_directory, "notes_done.html")
        end

        def basic_done_path
          File.join(working_directory, "basic_done.html")
        end

        def reduced_done_path
          File.join(working_directory, "reduced_done.html")
        end

      end
    end
  end
end
