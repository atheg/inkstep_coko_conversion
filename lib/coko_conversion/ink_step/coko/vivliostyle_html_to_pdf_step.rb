require 'coko_conversion/version'
require 'ink_step/conversion_step'

module InkStep::Coko
  class VivliostyleHtmlToPdfStep < InkStep::ConversionStep

    def perform_step
      super
      source_file_path = find_source_file(regex: parameter(:regex))
      source_file_name = Pathname(source_file_path).sub_ext ''
      output_file_path = File.join(working_directory, "#{source_file_name}.pdf")

      perform_conversion(source_file_path: source_file_path, output_file_path: output_file_path)
      success! if File.exists?(output_file_path)
    end

    def self.description
      "Runs vivliostyle conversion html to pdf. Template: vivliostyle-electron <input file> -o <output file>"
    end

    def self.human_readable_name
      "Vivliostyle HTML to PDF converter"
    end

    def version
      CokoConversion::VERSION
    end

    def required_parameters
      # e.g. [:foo, :bar]
      []
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {regex: "Regular expression for input file"}
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {regex: [/\.html$/, /\.htm$/]}
    end

    private

    def perform_conversion(source_file_path:, output_file_path:)
      combined_command = [
          set_xvfb_variable_command("DISPLAY", ":99.0"),
          run_xvfb_command,
          vivliostyle_command(File.join(working_directory, source_file_path), output_file_path)
      ].join("; ")

      perform_command(command: combined_command)
    end

    def set_xvfb_variable_command(key, value)
      "export #{key}='#{value}'"
    end

    def run_xvfb_command
      # Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &

      "Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1" #removed & to make it wait
    end

    def vivliostyle_command(source_file_path, destination_file_path)
      # see readme for more information about usage

      # vivliostyle-electron input.html -o output.pdf
      "vivliostyle-electron #{Shellwords.escape(source_file_path)} -o #{Shellwords.escape(destination_file_path)}"
    end
  end
end